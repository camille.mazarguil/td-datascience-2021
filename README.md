# td-datascience-2021

Sessions Machine et Deep Learning - module Agrotic/Datascience - 03/12 et 07/12

GPL-3 License INRAE/InstitutAgro MISTEA

les anti-sèches de:

* **keras**: https://raw.githubusercontent.com/rstudio/cheatsheets/master/keras.pdf

# Structure du repository

```
td-datascience-2021
|-> data
|-> tuto_install
|-> script_ml
README.md
```

# Quelques sites python de Machine learning

* https://scikit-learn.org/stable/
* https://scikit-learn.org/stable/tutorial/basic/tutorial.html
* https://www.tutorialspoint.com/scikit_learn/index.htm
* https://machinelearningmastery.com/machine-learning-in-python-step-by-step/
* https://makina-corpus.com/data-science/initiation-au-machine-learning-avec-python-la-theorie
* https://eric.univ-lyon2.fr/~ricco/cours/slides/PJ%20-%20machine%20learning%20avec%20scikit-learn.pdf

# Quelques sites python de Deep Learning

* https://sites.google.com/site/gdrecostat/accueil
* https://ecostat.gitlab.io/imaginecology/
* https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/classifierWithKeras/
* https://www.ionos.fr/digitalguide/sites-internet/developpement-web/keras-tutorial/
* https://machinelearningmastery.com/tutorial-first-neural-network-python-keras/
* https://keras.io/
* https://elitedatascience.com/keras-tutorial-deep-learning-in-python
* https://ecostat.gitlab.io/imaginecology/basics.html

# TD Machine Learning du 03/12/2021 - Sous-répertoire `scripts_ml`

Le sous-répertoire `scripts_ml` contient les fichiers .ipynb pour chaque groupe d'étudiants (5 groupes) pour le TD "Machine Learning" du 03/12/2021. Chaque groupe choisit une famille de modèle puis max 3 méthodes par famille et les mets en oeuvre sous Python avec **scikit-learn** (.ipynb). Une restitution par groupe sera faite en début de session le 07/12/2021.

Chaque sous-groupe se concentre sur une famille de méthodes mises en pratique sur le dataset **BloodBrain** mis à dispo dans le sous-rep `data`.

**BloodBrain** contient 134 variables prédictives et une variable réponse numérique (logBBB) sur 208 observations, on est donc dans une configuration de régression.

Nous proposons les 5 familles et donc 5 groupes suivants :

* random Forest: section Randomized Decision Trees dans https://www.tutorialspoint.com/scikit_learn/index.htm
* Feature Extraction: https://scikit-learn.org/stable/auto_examples/cross_decomposition/plot_pcr_vs_pls.html
* Feature Selection: https://scikit-learn.org/stable/modules/feature_selection.html#removing-features-with-low-variance
* L1 regularization: section Linear Modelling dans https://www.tutorialspoint.com/scikit_learn/index.htm
* SVM: section Support Vector Machines dans https://www.tutorialspoint.com/scikit_learn/index.htm


# TD Deep Learning du 07/12/2021

Nous partons des tutoriels de Miele _et. al._:

* https://imaginecology.sciencesconf.org/
* https://ecostat.gitlab.io/imaginecology/
* https://ecostat.gitlab.io/imaginecology/tutorials.html


## différents algos dans Keras

https://keras.io/api/applications/

## Analyse du tutoriel 7: un exemple de transfert learning

Classifying flower images with Keras (training + classification)


**Attention, le programme tourne sur le GPU, il faut donc changer l'algo utilisé pour que cela tourne sur le CPU. C'est expliqué dans le README**

* https://gitlab.com/ecostat/imaginecology/-/tree/master/projects/classifierWithKeras
* https://machinelearningmastery.com/transfer-learning-for-deep-learning/

## TD sur le perceptron

Classification de vins : http://archive.ics.uci.edu/ml/machine-learning-databases/wine

# Ressources en ligne sur le Deep learning

https://scikit-learn.org/stable/tutorial/basic/tutorial.html#

Dans ce tuto, il utilise le dataset **iris** sous python.

* https://oliviergimenez.github.io/prezDLoccupancy/#1

# Tutoriel d'installation des différents logiciels nécessaires

suivre la fiche tuto_install/Install_Outils_Agrotic.pdf.


# Ressources en ligne sur la PCA en pré-traitement: liens expliquant/utilisant PCA sur train et non le dataset complet

https://towardsdatascience.com/pca-using-python-scikit-learn-e653f8989e60

https://stats.stackexchange.com/questions/55718/pca-and-the-train-test-split

https://www.quora.com/Why-should-PCA-only-be-fit-on-the-training-set-and-not-the-test-set

https://stackabuse.com/implementing-pca-in-python-with-scikit-learn/

https://machinelearningmastery.com/principal-components-analysis-for-dimensionality-reduction-in-python/

donc: train/test avant puis ACP sur train puis fit sur les compos récupérées!
